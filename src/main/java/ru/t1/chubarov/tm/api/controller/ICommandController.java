package ru.t1.chubarov.tm.api.controller;

public interface ICommandController {

    void showInfo();

    void showArgumentError();

    void showCommandError();

    void showAbout();

    void showVersion();

    void showWelcome();

    void showCommands();

    void showArguments();

    void showHelp();

}
