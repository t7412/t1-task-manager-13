package ru.t1.chubarov.tm.api.service;

import ru.t1.chubarov.tm.api.repository.ITaskRepository;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.model.Task;

public interface ITaskService  extends ITaskRepository {

    Task create(String name, String description);

    void remove(Task task);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusById(String id, Status status);
}
